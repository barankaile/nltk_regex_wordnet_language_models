Detailed description of assignment can be found in "assignment_description.pdf"
hw2_part1.py uses Regular expressions to find desired elements present in corpora.
hw2_part2.py uses WordNet to find synsets, lemmas, definitions, and lexical relations.
hw2_part3.py normalizes data from a corpus containing tagged restaurant reviews, and creates a frequency distribution of tokens and whether they were more likely to appear in a positive or negative review.