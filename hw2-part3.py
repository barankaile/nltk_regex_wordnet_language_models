import re, nltk, argparse, sys
from nltk import *

def get_score(review):
    return int(re.search(r'Overall = ([1-5])', review).group(1))

def get_text(review):
    return re.search(r'Text = "(.*)"', review).group(1)

def read_reviews(file_name):
    """
    Dont change this function.

    :param file_name:
    :return:
    """
    file = open(file_name, "rb")
    raw_data = file.read().decode("latin1")
    file.close()

    positive_texts = []
    negative_texts = []
    first_sent = None
    for review in re.split(r'\.\n', raw_data):
        overall_score = get_score(review)
        review_text = get_text(review)
        if overall_score > 3:
            positive_texts.append(review_text)
        elif overall_score < 3:
            negative_texts.append(review_text)
        if first_sent == None:
            sent = nltk.sent_tokenize(review_text)
            if (len(sent) > 0):
                first_sent = sent[0]
    return positive_texts, negative_texts, first_sent


########################################################################
## Dont change the code above here
######################################################################



def process_reviews(file_name):
    positive_texts, negative_texts, first_sent = read_reviews(file_name)
    
    # There are 150 positive reviews and 150 negative reviews.
    
    negative_texts = [review.lower() for review in negative_texts]
    positive_texts = [review.lower() for review in positive_texts]
    #print(first_sent)
    
    stop_words = nltk.corpus.stopwords.words('english')
    #print(stop_words)
    
    #NEGATIVE
    for idx, item in enumerate(negative_texts):
        negative_texts[idx] = sent_tokenize(item)
    for idx, item in enumerate(negative_texts):
        for idy, sentence in enumerate(item):
            item[idy] = word_tokenize(sentence)
            
            
    for idx,review in enumerate(negative_texts):
        for idy,sentence in enumerate(review):
            negative_texts[idx] = [word for word in sentence if word not in stop_words]
    
    negative_texts = [re.sub(r'[^\w\s]', '', sentence) for review in negative_texts for sentence in review]
    negative_texts = [word for word in negative_texts if word]
    #print("Negative Lengths: " , len(negative_texts))
    most_common_negative = FreqDist(negative_texts).most_common()
    
    negative_bigrams = (bigrams(negative_texts))
    most_common_negative_bigrams = ConditionalFreqDist(negative_bigrams)
    
    #Unigrams
    sys.stdout = open('negative-unigram-freq.txt','w',encoding="utf-8")
    for word,count in most_common_negative:
        print(word, count)
        
    #Bigrams
    sys.stdout = open('negative-bigram-freq.txt','w',encoding="utf-8")
    bigram_counts = []
    
    
    for item in most_common_negative_bigrams.items():
        the_words = item[1].most_common()
        cond_word = item[0]
        for pair in the_words:
            bigram_counts.append((cond_word,pair[0],pair[1]))
            
    bigram_counts.sort(key=lambda tup: tup[2], reverse=True)
    for triple in bigram_counts:
        print(triple[0], triple[1], triple[2])
            
    #POSITIVE
    for idx, item in enumerate(positive_texts):
        positive_texts[idx] = sent_tokenize(item)
    for idx, item in enumerate(positive_texts):
        for idy, sentence in enumerate(item):
            item[idy] = word_tokenize(sentence)
        
    for idx,review in enumerate(positive_texts):
        for idy,sentence in enumerate(review):
            positive_texts[idx] = [word for word in sentence if word not in stop_words]
    
    positive_texts = [re.sub(r'[^\w\s]', '', sentence) for review in positive_texts for sentence in review]
    positive_texts = [word for word in positive_texts if word]
    
    
    most_common_positive = FreqDist(positive_texts).most_common()
    
    positive_bigrams = list(bigrams(positive_texts))
    most_common_positive_bigrams = ConditionalFreqDist(positive_bigrams)
    #Unigrams
    sys.stdout = open('positive-unigram-freq.txt','w',encoding="utf-8")
    for word,count in most_common_positive:
        print(word, count)
    #Bigrams
    sys.stdout = open('positive-bigram-freq.txt','w',encoding="utf-8")
    #most_common_negative_bigrams.tabulate()
    pos_bigram_counts = []
    for item in most_common_positive_bigrams.items():
        the_words = item[1].most_common()
        cond_word = item[0]
        for pair in the_words:
            pos_bigram_counts.append((cond_word,pair[0],pair[1]))
            
    pos_bigram_counts.sort(key=lambda tup: tup[2], reverse=True)
    for triple in pos_bigram_counts:
        print(triple[0], triple[1], triple[2])
    
    negative_texts = Text(negative_texts)
    positive_texts = Text(positive_texts)
    
    sys.stdout = sys.__stdout__
    print("Negative Text Collocations:")
    negative_texts.collocations()
    print("Positive Text Collocations:")
    positive_texts.collocations()
        


# Write to File, this function is just for reference, because the encoding matters.
def write_file(file_name, data):
    file = open(file_name, 'w', encoding="utf-8")    # or you can say encoding="latin1"
    file.write(data)
    file.close()


def write_unigram_freq(category, unigrams):
    """
    A function to write the unigrams and their frequencies to file.

    :param category: [string]
    :param unigrams: list of (word, frequency) tuples
    :return:
    """
    uni_file = open("{0}-unigram-freq-n.txt".format(category), 'w', encoding="utf-8")
    for word, count in unigrams:
        uni_file.write("{0:<20s}{1:<d}\n".format(word, count))
    uni_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Assignment 2')
    parser.add_argument('-f', dest="fname", default="restaurant-training.data",  help='File name.')
    args = parser.parse_args()
    fname = args.fname

    process_reviews(fname)
